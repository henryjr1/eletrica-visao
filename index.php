<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 16/01/2015
 * Time: 14:49
 */

include('includes/header.php');

if (isset($_GET['page'])) {
    switch ($_GET['page']) {
        case 'quemsomos':
            include_once('pages/QuemSomos.php');
            break;
        case 'clientes':
            include_once('pages/Clientes.php');
            break;
        case 'contatos':
            include_once('pages/Contatos.php');
            break;
        case 'eletrica':
            include_once('pages/Eletrica.php');
            break;
        case 'mecanica':
            include_once('pages/Mecanica.php');
            break;
        case 'assistencia':
            include_once('pages/Assistencia.php');
            break;
        case 'consulta':
            include_once('pages/Consulta.php');
            break;
        case 'servicos':
            include_once('pages/Services.php');
            break;
        default:
            include_once('pages/404.php');
    }
} else {
    include_once('pages/home.php'); // Página inicial
}

include('includes/footer.php');