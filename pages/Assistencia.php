<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 14/01/2015
 * Time: 21:40
 */
?>
<div class="container page servicos">
    <h2><span></span> SERVIÇOS / MECÂNICA</h2>

    <a href="/?page=eletrica#1" class="row">
        <div class="col-md-3 title">ELÉTRICA</div>
        <div class="col-md-9 bg1"></div>
    </a>

    <div class="space25"></div>
    <a href="/?page=mecanica#2" class="row">
        <div class="col-md-3 title">MECÂNICA</div>
        <div class="col-md-9 bg2"></div>
    </a>

    <div class="row">
        <div class="col-md-3 title" name="3" id="3">ASSISTÊNCIA<br/>TÉCNICA</div>
        <div class="col-md-9 bg3active"></div>
    </div>
    <div class="space25"></div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>DIAGNÓSTICO ELÉTRICO EM MÁQUINAS
                ELÉTRICAS ROTATIVAS E TRANSFORMADORES</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Elaboramos para o cliente uma curva de tendência de falha que determina a melhor época em que o equipamento pode ser colocado em
                manutenção, visando utilizar o equipamento ao máximo com reparo a um custo menor.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>REPARO EM MOTORES ELÉTRICOS CA E CC
                DE ALTA E BAIXA TENSÃO</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>No Norte e Nordeste a Elétrica Visão é um dos maiores assistentes técnicos da Weg, a principal fabricante de motores do Brasil e uma
                das maiores do mundo. Temos estrutura física e corpo técnico adequado para realização de trabalhos como rejuvenescimento,
                rebobinamento, repotenciamento, readequação, manutenção preventiva e corretiva. Além de reparos em geradores de corrente contínua e
                alternada. Também comercializamos motores Weg.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>REPARO EM BOMBAS</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>A Elétrica Visão é assistente técnico da Xylem e da Schneider, empresas que detém boa parte do mercado brasileiro voltado tanto para o
                mercado industrial quanto doméstico, além de parceria com várias outras empresas de referência. Somos multimarcas no reparo de bombas
                centrífugas, mancalizadas, submersas e submersíveis. Realizamos rejuvenescimento, balanceamento, troca de rolamentos e sistema de
                vedação, testes e pintura do equipamento, entre outros serviços. </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>CONSULTORIA E TREINAMENTO EM APLICAÇÃO DE MOTORES</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Temos a preocupação de que o nosso cliente faça um bom uso da sua planta industrial e para isso disponibilizamos consultorias em várias
                áreas, como “Confiabilidade de estoque” (manutenção de motores em estoque); “Dimensionamento de estoque”, “Eficiência energética e
                conservação de energia”, “Análise de falha”, “Estudo de intercambialidade em motores elétricos” e “Comissionamento e start-up”. Além
                disso, há treinamentos ministrados na Weg, em Santa Catarina ou São Paulo; na própria Elétrica Visão ou na empresa do cliente. Alguns
                contam com a participação de fabricantes com os quais trabalhamos.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>OUTROS SERVIÇOS SOB CONSULTA</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Montagem elétrica, automação e controle industrial, instrumentação e manutenção industrial. </p>
        </div>
    </div>
