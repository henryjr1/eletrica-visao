<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 14/01/2015
 * Time: 21:39
 */
?>
<div class="container page quemsomos">
    <h2><span></span>QUEM SOMOS</h2>

    <div class="row">
        <div class="col-md-3">
            <p><img src="/assets/images/PageImg1.jpg" class="img-responsive"></p>

            <p><img src="/assets/images/PageImg2.jpg" class="img-responsive"></p>
        </div>
        <div class="col-md-7">
            <p>A Elétrica Visão é um dos maiores Assistentes Técnicos da Weg no Norte e Nordeste do Brasil. Especializada no reparo de motores
                elétricos, a Elétrica Visão reúne hoje a melhor infraestrutura de oficina, equipamentos e corpo técnico no Maranhão. Com foco nas
                grandes empresas industriais de setores como alimentício, construção, metalurgia, siderurgia e portuário, tem um leque de opções em
                serviços elétricos, mecânicos e de assistência técnica voltados para máquinas elétricas rotativas, como motores (corrente contínua
                e alternada), geradores, painéis, além de transformadores, calderaria, usinagem e balanceamento. A Elétrica Visão também se tornou
                referência no estado para equipamentos de uso doméstico e comercial em condomínios, supermercados, hospitais, hotéis e shopping
                centers.</p>

            <p>Com flexibilidade nos preços e locais de atendimento, grande rede de fornecedores e parceiros, a Elétrica Visão oferece um vasto
                cardápio de serviços que pode ser prestado sob consulta e na medida certa para cada cliente. Agende uma visita com nossos
                técnicos que, com certeza, teremos uma solução para a sua empresa em serviços como montagens de subestações; consultoria em
                eficiência energética e conservação de energia; fabricação e montagem de quadros elétricos; montagem, comissionamento, start-up em
                transformadores e em máquinas elétricas rotativas; montagem mecânica; análise de vibração; diagnóstico elétrico em máquinas
                elétricas rotativas, transformadores e outros.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-6"><img src="/assets/images/marcas/procem.jpg" class="img-responsive"></div>
        <div class="col-md-4 col-sm-6"><img src="/assets/images/marcas/iso9001.jpg" class="img-responsive"></div>
        <div class="col-md-4 col-sm-6"><img src="/assets/images/marcas/icema.jpg" class="img-responsive"></div>
    </div>

    <h2><span></span>NOSSA HISTÓRIA</h2>

    <div class="col-md-7 col-md-offset-3">
        <p><img src="/assets/images/NossaHistoria.jpg" class="img-responsive"></p>

        <p>Fundada há 22 anos, a Elétrica Visão atua no ramo de rebobinamento de motores elétricos de indução, recuperação de transformadores,
            manutenção de geradores de pequeno e grande porte, além de bombas centrifugas e máquinas de solda, entre outros. No Maranhão, a Elétrica
            Visão possui a melhor estrutura de oficina de reparo de motores, tanto em espaço e equipamentos quanto em pessoal treinado.</p>

        <p>A empresa está instalada no Distrito Industrial de São Luís em uma área própria de mais de 20.000m², estrategicamente mais próxima das
            indústrias locais. A Elétrica Visão é certificada pela ISO 9001:2008 desde 2010, assim como pelo Programa de Certificação de Empresas
            Maranhenses (Procem).</p>

        <p>Fundada por um visionário, Nazareno Andrade dos Santos, a empresa continua associando o seu crescimento à implantação de programas de
            qualidade, de segurança e de respeito ao meio ambiente, aos seus colaboradores, clientes e à comunidade do entorno. Nazareno, autodidata,
            foi pioneiro na recuperação de transformadores no Maranhão. Antes, o filho de lavrador também lavrou a roça, foi caminhoneiro, topógrafo,
            técnico em montagens e gerente de empresas. Duas décadas depois, a empresa segue firme com a visão de ser a melhor no estado e para tanto
            vem diversificando a sua atuação para atender, além da indústria também as demandas do comércio.</p>
    </div>