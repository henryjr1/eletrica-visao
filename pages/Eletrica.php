<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 14/01/2015
 * Time: 21:40
 */
?>
<div class="container page servicos">
    <h2><span></span> SERVIÇOS / ELÉTRICA</h2>

    <div class="row">
        <div class="col-md-3 title" name="1" id="1">ELÉTRICA</div>
        <div class="col-md-9 bg1active"></div>
    </div>
    <div class="space50"></div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>MONTAGEM DE SUBESTAÇÕES</h4></div>
    </div>
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <p>Fornecimento de mão de obra técnica especializada na montagem de subestações em parceria com grandes
                fabricantes. A nossa empresa é responsável pela montagem mecânica, comissionamento, start-up de
                transformadores, chaves, painéis, disjuntores e pela montagem mecânica, o que inclui tratamento de óleo
                de transformadores e determinados trabalhos de construção civil. Também fornecemos recursos como
                caminhões munk, equipamentos para realização de testes, montagem e materiais para içamento. Rigor na
                execução do projeto, atenção às normas internacionais, parceria com o cliente e facilidades de
                manutenção são algumas das marcas da Elétrica Visão na montagem de subestações.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>CONSULTORIA EM EFICIÊNCIA ENERGÉTICA E CONSERVAÇÃO DE ENERGIA</h4></div>
    </div>
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <p>O objetivo é ajudar nossos clientes a ter o controle efetivo dos custos da produção. Com a redução do
                consumo de energia elétrica em
                seus parques industriais, um dos maiores custos na produção, ampliamos ao máximo os seus resultados.
                Nossa consultoria analisa a
                qualidade da energia utilizada para evitar que o cliente tenha perdas no consumo de energia, otimizando
                o seu uso e tornando-se mais
                eficiente.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>FABRICAÇÃO E MONTAGEM DE QUADROS ELÉTRICOS</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>De acordo com projeto específico fornecido pelo cliente, fabricamos, instalamos e acompanhamos start-up
                na planta.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>CONSULTORIA EM GESTÃO ELÉTRICA</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>A Elétrica Visão tem expertise em assumir uma planta industrial com toda a gestão elétrica para que o
                cliente tenha confiabilidade e
                disponibilidade integral de sua planta. A consultoria avalia os pontos fortes e fracos, identifica e
                implementa melhorias na gestão
                elétrica. </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>MONTAGEM, COMISSIONAMENTO, START-UP EM TRANSFORMADORES E EM MÁQUINAS ELÉTRICAS
                ROTATIVAS</h4></div>
    </div>
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <p>Por meio do comissionamento asseguramos que os sistemas e componentes de uma planta industrial sejam
                mantidos de acordo com as
                necessidades e requisitos operacionais demandados pelo cliente. O comissionamento pode ser aplicado a
                equipamentos novos ou não, a
                sistemas existentes ou ainda a projetos de expansão, modernização ou que necessitem de ajustes.
                Realizamos montagem, testes prévios,
                tratamento e análise de óleo, entre outros serviços. Bem planejado, esse trabalho evita perda de tempo,
                de produção e do próprio
                equipamento.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>MANUTENÇÃO EM MÁQUINAS DE SOLDA</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Possui equipe especializada para trabalhar com manutenção preventiva e corretiva de máquinas de solda
                multimarcas, sempre com a
                utilização de peças genuínas para reposição. Entre outros benefícios, a manutenção em máquinas de solda
                aumenta a segurança, evita
                falhas inesperadas e aumenta a produtividade. </p>
        </div>
    </div>

    <div class="space50"></div>
    <a href="/?page=mecanica#2" class="row">
        <div class="col-md-3 title">MECÂNICA</div>
        <div class="col-md-9 bg2"></div>
    </a>

    <div class="space25"></div>
    <a href="/?page=assistencia#3" class="row">
        <div class="col-md-3 title">ASSISTÊNCIA<br/>TÉCNICA</div>
        <div class="col-md-9 bg3"></div>
    </a>