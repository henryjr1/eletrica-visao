<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 13/01/2015
 * Time: 17:29
 */
?>

<!-- Carousel
================================================== -->
<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/assets/images/SliderHome1.jpg" data-src="/assets/images/SliderHome1.jpg" alt="First slide">

                <div class="container">
                    <div class="carousel-caption">
                        <h1>MANUTENÇÃO DE MOTORES ELÉTRICOS</h1>

                        <p>PARA GRANDES E PEQUENAS INDÚSTRIAS</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="/assets/images/SliderHome2.jpg" data-src="/assets/images/SliderHome2.jpg" alt="Second slide">

                <div class="container">
                    <div class="carousel-caption">
                        <h1>TÉCNICOS CAPACITADOS</h1>

                        <p>PARA LHE ATENDER COM QUALIDADE</p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
</div>


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container">

    <!-- Servicos -->
    <div class="row text-center square">
        <h2>CONHEÇA NOSSOS PRINCIPAIS SERVIÇOS</h2>
    </div>
    <div class="areas">
        <div class="row">
            <div class="col-lg-4 eletrica">
                <h2>ELÉTRICA</h2>

                <p>Consultoria, fabricação, montagem, comissionamento e manutenção de máquinas.</p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-sm-6 col-xs-12 mecanica">
                <h2>MECÂNICA</h2>

                <p>Montagem mecânica,análise de vibração,manutenção, caldeiraria e usinagem.</p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4 col-sm-6 col-xs-12 assistencia">
                <h2>ASSISTÊNCIA<BR>TÉCNICA</h2>

                <p>Diagnóstico elétrico em máquinas, reparo em motores elétricos CA/CC e reparo em bombas.</p>
            </div>
            <!-- /.col-lg-4 -->
        </div>
    </div>
    <!-- /.servicos -->
    <div class="space75"></div>
    <div class="space100"></div>
    <!-- xtras -->
    <div class="xtras ">
        <div class="row text-center">
            <div class="col-md-4 col-sm-6 col-xs-6">
                <h2><i class="fa fa-thumbs-o-up fa-3x"></i></h2>
                <h4>AGILIDADE<br> NO ATENDIMETO</h4>

                <p>Com elasticidade na infraestrutura, assim como as inovações tecnológicas constantes, e uma boa cadeia de fornecedores, executamos
                    os serviços de maneira efetiva e, acima de tudo, ágil.</p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <h2><i class="fa fa-check-circle-o fa-3x"></i></h2>
                <h4>QUALIDADE<br> NO SERVIÇO</h4>

                <p>A Elétrica Visão preza pelo crescimento e desenvolvimento sustentável, a fim de garantir a excelência em qualidade na prestação de
                    serviços.</p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6">
                <h2><i class="fa fa-leaf fa-3x"></i></h2>
                <h4>COMPROMISSO<br> AMBIENTAL</h4>

                <p>Respeitar o meio ambiente faz parte dos valores de uma empresa. A Elétrica Visão se preocupa em conscientizar seus funcionários e
                    outras empresas do uso correto de matérias-primas.</p>
            </div>
            <!--            <div class="col-md-3 col-sm-6 col-xs-6">-->
            <!--                <h2><i class="fa fa-users fa-3x"></i></h2>-->
            <!--                <h4>RESPONSABILIDADE<br> SOCIAL</h4>-->
            <!---->
            <!--                <p>A Elétrica Visão firma o compromisso com pessoas e valores humanos, sendo funcionários, comunidades e parceiros, transparecendo-->
            <!--                    ética e postura responsável.</p>-->
            <!--            </div>-->
        </div>
    </div>
    <!-- /.xtras -->
    <div class="row">
        <div class="col-lg-12 assistencia">
            <img src="/assets/images/assistencia.png" class="img-responsive text-center">
        </div>
    </div>
