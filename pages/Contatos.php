<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 14/01/2015
 * Time: 21:39
 */
?>
<div class="container page contatos">
    <h2><span></span> CONTATOS</h2>

    <?php
    require("PHPMailer/class.phpmailer.php");
    $mail = new PHPMailer();
    if (isset($_POST['env'])) {
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = "mail.gmail.com"; // SMTP server
        $mail->SMTPDebug = 2;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
        $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "hmjsite@gmail.com";  // GMAIL username
        $mail->Password = "HenryJr123";            // GMAIL password

        $mail->AddReplyTo('eletricavisao@eletricavisao.com.br', 'Elétrica Visão');
        $mail->AddAddress($_POST['email'], $_POST['nome']);
        $mail->SetFrom('eletricavisao@eletricavisao.com.br', 'Elétrica Visão');
        $mail->AddReplyTo('contato@eletricavisao.com.br', 'Elétrica Visão');
        $mail->Subject = '[Elétrica Visão] Contato via site';
        $mail->Body = $_POST['mensagem'];
        $mail->AltBody = 'Empresa: ' . $_POST['empresa'] . ', Telefone: ' . $_POST['telefone']; // optional - MsgHTML will create an alternate automatically
        if (!$mail->Send()) {
            echo 'Não enviada. \n';
            echo 'Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Enviada.';
            die();
        }
    }
    ?>

    <div class="row">
        <div class="col-md-9">
            <form action="/?page=Contatos" method="post">
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="nome" class="control-label">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Seu nome" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="nome" class="control-label">Empresa</label>
                        <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Sua empresa" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="nome" class="control-label">Telefone</label>
                        <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="Seu telefone" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="email" class="control-label">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Seu e-mail" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="mensagem" class="control-label">Sua mensagem</label>
                        <textarea class="form-control" rows="3" id="mensagem" name="mensagem" required></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="text-right col-md-8">
                        <input type="hidden" id="env" name="env" value="1">
                        <button type="submit" class="btn btn-warning">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <div class="row">
                <img src="/assets/images/tel.jpg">
            </div>
            <div class="row">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3985.612892043428!2d-44.259890999999996!3d-2.6309000000000005!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMsKwMzcnNTEuMiJTIDQ0wrAxNSczNS42Ilc!5e0!3m2!1spt-BR!2s!4v1425414638538"
                    width="278" height="220" frameborder="0" style="border:5px solid #D7D9DD;"></iframe>
            </div>
        </div>
    </div>