<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 20/01/2015
 * Time: 12:10
 */
?>
<div class="container page servicos">
    <h2><span></span> SERVIÇOS</h2>

    <a href="/?page=eletrica#1" class="row" name="1">
        <div class="col-md-3 title">ELÉTRICA</div>
        <div class="col-md-9 bg1"></div>
    </a>

    <div class="space25"></div>
    <a href="/?page=mecanica#2" class="row" name="2">
        <div class="col-md-3 title">MECÂNICA</div>
        <div class="col-md-9 bg2"></div>
    </a>

    <div class="space25"></div>
    <a href="/?page=assistencia#3" class="row" name="3">
        <div class="col-md-3 title">ASSISTÊNCIA<br/>TÉCNICA</div>
        <div class="col-md-9 bg3"></div>
    </a>