<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 14/01/2015
 * Time: 21:40
 */
?>
<div class="container page servicos">
    <h2><span></span> SERVIÇOS / MECÂNICA</h2>

    <a href="/?page=eletrica#1" class="row">
        <div class="col-md-3 title">ELÉTRICA</div>
        <div class="col-md-9 bg1"></div>
    </a>

    <div class="space25"></div>
    <div class="row">
        <div class="col-md-3 title" name="2" id="2">MECÂNICA</div>
        <div class="col-md-9 bg2active"></div>
    </div>

    <div class="space25"></div>

    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>MONTAGEM MECÂNICA</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Dispõe de equipe para montagens mecânicas de acordo com o escopo determinado pelo projeto do cliente, a exemplo de tubulações, máquinas
                elétricas rotativas, geradores, motores, estruturas mecânicas em geral.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>ANÁLISE DE VIBRAÇÃO</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Diagnóstico mecânico em componentes, especialmente rolamentos, visando detectar possíveis problemas de desbalanceamento de mancais em
                máquinas elétricas rotativas, redutores, ventiladores, bombas etc.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>REDUTORES</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Manutenção mecânica do redutor com a utilização de peças genuínas, de acordo com o fabricante; recomposição de formas e pintura.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>FREIOS ELETROMECÂNICOS</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Desmontagem, peritagem, avaliação elétrica e mecânica dos freios, reparos estruturais, montagem, teste e pintura.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>CALDEIRARIA</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Fabricação de ventiladores, bases, tampas em chapa sob desenho.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 barBlue"></div>
        <div class="col-md-10"><h4>USINAGEM DE PEQUENO PORTE</h4></div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p>Fabricação e reparo de componentes.</p>
        </div>
    </div>

    <div class="space50"></div>
    <a href="/?page=assistencia#3" class="row">
        <div class="col-md-3 title">ASSISTÊNCIA<br/>TÉCNICA</div>
        <div class="col-md-9 bg3"></div>
    </a>