<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 16/01/2015
 * Time: 14:50
 */
?>

<div class="space100"></div>
<!-- FOOTER -->
<footer>
    <div class="row barTop">
        <div class="col-md-7 barTop1"><p>AV. 5, MÓDULO A, QUADRA E, N.º 5 – DISTRITO INDUSTRIAL – SÃO LUÍS/MA – CEP: 65.090-272</p></div>
        <div class="col-md-3 barTop2"><a href="http://www.facebook.com" target="_blank" title="Fanpage"><i class="fa fa-facebook-square"></i></a>
            &middot; <a href="https://www.google.com/maps/@-2.6309,-44.259891,3a,75y,133.65h,89.66t/data=!3m4!1e1!3m2!1sULWVrfKZaJDu6GKn8dXH1g!2e0"
                        target="_blank" title="Google Maps"><i class="fa fa-map-marker"></i></a>
            &middot; <a href="/?page=contatos" title="Contatos"><i class="glyphicon glyphicon-envelope"></i></a></div>
        <div class="col-md-2 barTop3">(98) 2109 4500<p class="pull-right"><a href="#" title="Topo"><i class="fa fa-arrow-up"></i></a></p></div>
    </div>
</footer>

</div><!-- /.container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>