<?php
/**
 * Created by IntelliJ IDEA.
 * User: Henrique
 * Date: 16/01/2015
 * Time: 14:59
 */
?>

<div class="navbar-wrapper">
    <div class="container">
        <div class="row barTop">
            <div class="col-md-7 barTop1"></div>
            <div class="col-md-3 barTop2"></div>
            <div class="col-md-2 barTop3"></div>
        </div>
        <div class="row">
            <div class="col-md-6"><a href="/"><img src="/assets/images/eletricavisao.png"></a></div>
            <div class="col-md-6">
                <nav class="navbar navbar-default">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                                    aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse navbar-right navbase">
                            <ul class="nav navbar-nav">
                                <li><a href="/?page=quemsomos">QUEM SOMOS</a></li>
                                <li><a href="/?page=servicos">SERVIÇOS</a></li>
                                <!--                                <li class="dropdown">-->
                                <!--                                    <a href="/?page=servicos"-->
                                <!--                                       aria-expanded="auto">SERVIÇOS <span class="caret"></span></a>-->
                                <!--                                    <ul class="dropdown-menu" role="menu">-->
                                <!--                                        <li><a href="/?page=eletrica">Elétrica</a></li>-->
                                <!--                                        <li><a href="/?page=mecanica">Mecânica</a></li>-->
                                <!--                                        <li><a href="/?page=assistencia">Assistência</a></li>-->
                                <!--                                    </ul>-->
                                <!--                                </li>-->
                                <li><a href="/?page=clientes">CLIENTES</a></li>
                                <li><a href="/?page=contatos">CONTATO</a></li>
                            </ul>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse navbar-right webmail">
                            <ul class="nav navbar-nav">
                                <li><a href="http://webmail.eletricavisao.com.br" target="_blank"><i class="glyphicon glyphicon-envelope"></i> WebMail</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>